from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from rest_framework.authtoken import views

from users.views import UserViewSet
from videos.views import VideoViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'videos', VideoViewSet)

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/token-auth/', views.obtain_auth_token),
    url(r'^api/', include(router.urls)),

]
