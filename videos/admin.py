from django.contrib import admin

from videos.models import Video, Rent


class VideoAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'is_available']


class RentAdmin(admin.ModelAdmin):
    list_display = ['id', 'video', 'user', 'rental_date']

admin.site.register(Video, VideoAdmin)
admin.site.register(Rent, RentAdmin)
