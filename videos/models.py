from django.contrib.auth.models import User
from django.db import models


class Video(models.Model):
    title = models.CharField(max_length=64)
    is_available = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.title)


class Rent(models.Model):
    user = models.ForeignKey(User, related_name="rents")
    video = models.OneToOneField(Video, related_name="rent")
    rental_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} leased by {}".format(self.video, self.user.username)
