# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def initial_data(apps, schema_editor):
    video_model = apps.get_model('videos', 'Video')
    for i in range(1, 11):
        video_model.objects.create(title="Title {}".format(i))


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(initial_data)
    ]
