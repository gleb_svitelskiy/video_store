from rest_framework import serializers

from videos.models import Video, Rent


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('id', 'title', 'is_available')


class RentSerializer(serializers.ModelSerializer):
    video_id = serializers.IntegerField(source="video.id")
    user_id = serializers.IntegerField(source="user.id")

    class Meta:
        model = Rent
        fields = ('id', 'video_id', 'user_id', 'rental_date')
