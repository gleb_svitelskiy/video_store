# video_store project

## API HTTP methods

### Register user

    curl -X POST -H "Cache-Control: no-cache" -H "Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" -F "username=user1" -F "email=user1@localhost" -F "password=user1password" "http://127.0.0.1:8000/api/users/"

### Get all users

    curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/users/"

### Token authentication

    curl -X POST -H "Cache-Control: no-cache" -H "Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" -F "username=user1" -F "password=user1password" "http://127.0.0.1:8000/api/token-auth/"

### Get user data

    curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/users/1/"

### Update user data

    curl -X PATCH -H "Authorization: Token d689d9b9b491950b5044a0be708d246db177e623" -H "Cache-Control: no-cache" -H "Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" -F "email=new_email@localhost" "http://127.0.0.1:8000/api/users/1/"

### Delete user

    curl -X DELETE -H "Authorization: Token d689d9b9b491950b5044a0be708d246db177e623" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/users/1/"

### Get all videos

    curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/videos/"

### Search video by title

    curl -X GET -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/videos/?search=1"

### Rent the video

    curl -X POST -H "Authorization: Token d689d9b9b491950b5044a0be708d246db177e623" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/videos/1/rent/"

### Return the video

    curl -X POST -H "Authorization: Token d689d9b9b491950b5044a0be708d246db177e623" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/videos/1/return/"
