# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0002_auto_20170123_2049'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rent',
            name='video',
            field=models.OneToOneField(related_name='rent', to='videos.Video'),
        ),
    ]
