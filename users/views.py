from django.contrib.auth.models import User
from rest_framework import viewsets

from users.serializers import UserSerializer
from video_store.permissions import IsSelfOrCreateOrReadOnly


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsSelfOrCreateOrReadOnly,)
