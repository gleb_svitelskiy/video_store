from django.db import IntegrityError
from django.db import transaction
from rest_framework import filters
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.exceptions import APIException
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from videos.models import Video, Rent
from videos.serializers import VideoSerializer, RentSerializer


class VideoViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
    filter_backends = (filters.SearchFilter, )
    search_fields = ('title', )

    @detail_route(methods=['post'], url_path='rent', permission_classes=[IsAuthenticated])
    def rent_video(self, request, *args, **kwargs):
        video = self.get_object()
        if not video.is_available:
            raise APIException('Video is not available for rent', status.HTTP_400_BAD_REQUEST)

        try:
            with transaction.atomic():
                video.is_available=False
                video.save()
                rent = Rent()
                rent.video = video
                rent.user = request.user
                rent.save()
        except IntegrityError:
            raise APIException('Video rent error')
        else:
            return Response(RentSerializer(instance=rent).data)

    @detail_route(methods=['post'], url_path='return', permission_classes=[IsAuthenticated])
    def return_video(self, request, *args, **kwargs):
        video = self.get_object()

        if video.is_available:
            raise APIException('Video is in store', status.HTTP_400_BAD_REQUEST)
        elif hasattr(video, 'rent') and (request.user != video.rent.user):
            raise APIException("You did't rent this video", status.HTTP_400_BAD_REQUEST)

        try:
            with transaction.atomic():
                if hasattr(video, 'rent'):
                    video.rent.delete()
                video.is_available = True
                video.save()
        except IntegrityError:
            raise APIException('Video return error')
        else:
            return Response('Video returned')
