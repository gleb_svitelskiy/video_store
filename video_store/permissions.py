from rest_framework import permissions
from rest_framework.compat import is_authenticated
from rest_framework.permissions import SAFE_METHODS


class IsSelfOrCreateOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return (
            request.method in SAFE_METHODS or
            view.action == 'create' or
            request.user and is_authenticated(request.user)
        )

    def has_object_permission(self, request, view, obj):
        return (
            request.method in SAFE_METHODS or
            obj.id == request.user.id
        )
